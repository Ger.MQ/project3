#ifndef FIXEDARRAYSTACK_H
#define FIXEDARRAYSTACK_H

#include "Stackinterface.h"

#define N 5

class FixedArrayStack : public Stackinterface
{
    public:
        FixedArrayStack();
        virtual ~FixedArrayStack();
        virtual StackableObject * pop();
        virtual bool push(StackableObject *o);
        virtual int getCount();

    private:
        int indice;
        StackableObject *pila[N];
};

#endif // FIXEDARRAYSTACK_H
