#ifndef STACKNODE_H
#define STACKNDE_H
#include "Stackinterface.h"
#include <iostream>

class StackNode
{
    private:
        
		StackableObject* object;
		StackNode*       prevNode;
    
	public:
        StackNode(StackableObject* object = NULL ,StackNode* prevNode = NULL){
                this->object = object;
                this->prevNode = prevNode;
        }
		StackNode* getPrevNode()
		{
			return prevNode;
		}

		void setPrevNode(StackNode* prevNode)
		{
			this->prevNode = prevNode;
		}

		StackableObject* getObject()
		{
			return object;
		}
	
};
 #endif