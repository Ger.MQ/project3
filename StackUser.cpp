#include <iostream>
#include "StackUser.h"
#include "StackableObject.h"
#include "Vehiculo.h"

StackUser::StackUser(Stackinterface* _stack):stack(_stack){}

void StackUser::cargarPila()
		{   
            int aux = 0, i = 1;
            int ruedas;
            bool t_baul;
            float cil;
           
            std::cout <<"Ingresar nro de vehiculos a cargar: ";
            std::cin >> aux;
           
            while(aux >= i){
                 std::cout<<"\n...........vehiculo:"<<i<<"...............\n";
                 std::cout<<"cant. ruedas:";
                 std::cin>>ruedas;
                 std::cout<<"tiene baul? (1 = si or 0 = no):";
                 std::cin>>t_baul;
                 std::cout<<"cilindrada: ";
                 std::cin>>cil;    
               stack->push(new Vehiculo(ruedas,t_baul,cil));
               if(stack->getCount() == N) std::cout<<"Pila llena, caso arreglo estatico \n"; 
            i++;
		  }
      
        }

void StackUser::imprimirPila(){

			StackableObject* o;
            int i = stack->getCount();
			std::cout << "nro. de vehiculos en la pila: " << (stack->getCount());
            std::cout <<"\nElementos de la Pila:\n";
			while ((o = stack->pop()) != nullptr)
			{   
                std::cout<<"\n...........vehiculo:"<<i<<"...............\n";
				o->mostrarEnPantalla();
                i--;
			}
            std::cout <<"___________________________________________\n";
			std::cout << "nro. de vehiculos en la pila: " << (stack->getCount()) << "\n";
		}