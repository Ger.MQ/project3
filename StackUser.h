
#ifndef STACKUSER_H
#define STACKUSER_H
#include "Stackinterface.h"
#include "FixedArrayStack.h"

class StackUser
{
    public:
        StackUser(Stackinterface * stack);
        virtual ~StackUser(){};

        void cargarPila();
        void imprimirPila();

    private:
        Stackinterface * stack;
};

#endif // STACKUSER_H
