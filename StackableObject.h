
#ifndef STACKABLEOBJECT_H
#define STACKABLEOBJECT_H
//#include "Stackinterface.h"

class StackableObject
{
	public:
		StackableObject() {}

		~StackableObject() {}

		virtual void mostrarEnPantalla() = 0;
};

#endif // STACKABLEOBJECT_H
