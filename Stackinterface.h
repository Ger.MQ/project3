
#ifndef STACKiNTERFACE_H
#define STACKiNTERFACE_H
#include "StackableObject.h"

class Stackinterface{  //clase base 2
	public:
		Stackinterface() {}
		~Stackinterface() {}
		virtual bool push(StackableObject*) = 0;
		virtual StackableObject* pop() = 0;
		virtual int getCount() = 0;
};

#endif 