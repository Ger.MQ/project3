
#include <iostream>
#include "Vehiculo.h"

Vehiculo::Vehiculo(int r,bool baul,float c){
			cantRuedas = r;
			tieneBaul = baul;
			cilindrada = c;
}
void Vehiculo::mostrarEnPantalla(){
			std::cout << "Nro. de ruedas: " << cantRuedas << "\n";
            std::cout << "Tiene ruedas: ";
			          if(getTieneBaul()) 
                      {std::cout<<"si\n";} else{std::cout<<"no\n";}
            std::cout<< "Cilindrada: " << cilindrada << "\n";
		}
int Vehiculo::getCantRuedas(){	return cantRuedas;}
bool Vehiculo::getTieneBaul(){ return tieneBaul;}
float Vehiculo::getCilindrada(){ return cilindrada;}
void Vehiculo::setCantRuedas(int val){	cantRuedas =  val;}
void Vehiculo::setTieneBaul(bool val){ tieneBaul = val;}
void Vehiculo::setCilindrada(float val){ cilindrada = val;}