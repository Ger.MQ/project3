#ifndef VEHICULO_H
#define VEHICULO_H
#include "StackableObject.h"

class Vehiculo : public StackableObject
{
	public:
		Vehiculo(int, bool, float);
		void mostrarEnPantalla();
		int getCantRuedas();
		bool getTieneBaul();
        float getCilindrada(); 
	    void setCantRuedas(int);
		void setTieneBaul(bool);
		void setCilindrada(float);
	private:
		int   cantRuedas;
		bool  tieneBaul;
		float cilindrada;
};
#endif 
