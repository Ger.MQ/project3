#include "Stackinterface.h"
#include "FixedArrayStack.h"
#include "DynamicMemStack.h"
#include "StackUser.h"
#include <iostream>


int main()
{
    unsigned x;
    std::cout<<"Escoger 1 o 2 segun corresponda:\n";
    std::cout << "(1)Pila de tamaño fijo "<< N <<", con arreglo estatico:\n";
	std::cout << "(2)Pila dinámica con lista enlazada:\n"; 
    std::cin >>x;

    if(x==1){
        FixedArrayStack stack;
        StackUser user(&stack);
        user.cargarPila();
        system("clear"); 
	    user.imprimirPila();
	    std::cout << '\n';
    }
    else if(x==2){

         DynamicMemStack stack;
         StackUser user(&stack);
         user.cargarPila();
         system("clear"); 
	     user.imprimirPila();
	     std::cout << '\n';
         }
         else{  
            std::cout << "Valor no valido"<< std::endl;
         }
    
    return 0;
}
